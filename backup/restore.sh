#!/bin/bash

HOME="/home/marck/test_backup"

source ".env"

echo "Restore started"

export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
export RESTIC_REPOSITORY
export RESTIC_PASSWORD


restic restore latest --target $1 

echo "Unseting env. variables"

unset RESTIC_REPOSITORY
unset RESTIC_PASSWORD
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
