# Exclude files from backup
.cache
*.swp
*pyc
./**/__pycache__/
#*.log

