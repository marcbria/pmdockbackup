#!/bin/bash

HOME="/home/marck/test_backup"

# Use docker-compose environment file
source ".env"

echo "Vars Loaded"

export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
export RESTIC_REPOSITORY
export RESTIC_PASSWORD
