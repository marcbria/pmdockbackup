#!/bin/bash

#MAILTO="example.email@gmail.com"
#CRON_SCHEDULE=0 1 * * * # every day
# 2>&1

echo "[+] Backup started"

# Relative or absolute path
TO_BACKUP="$PWD"

echo "[+] Backuping $TO_BACKUP"

# Use docker-compose environment file to import variables
source ".env"

. backup/load_vars.sh

echo "[+] Stoping containers"

#TODO: make a loop to stop multiple DB containers

# Stoping containers with a DB to maintaint DB coherence
docker-compose stop $MYSQL_HOST
docker-compose pause

restic backup $TO_BACKUP --exclude-file=backup/excludes.txt --tag $COMPOSE_PROJECT_NAME

echo "[+] Re-starting Containers"

docker-compose unpause
docker-compose up -d

echo "[+] Unseting env. variables"

unset RESTIC_REPOSITORY
unset RESTIC_PASSWORD
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
